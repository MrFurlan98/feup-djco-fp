using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampModel : MonoBehaviour
{
    public int numTotalEnemies;
    private List<GameObject> enemies = new List<GameObject>();
    private List<Vector3> enemiesSpawnPositions = new List<Vector3>();

    public List<GameObject> Enemies { get => enemies; set => enemies = value; }
    public List<Vector3> EnemiesSpawnPositions { get => enemiesSpawnPositions; set => enemiesSpawnPositions = value; }
}
