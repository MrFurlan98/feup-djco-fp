using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camp : MonoBehaviour
{
    public Transform target;
    public float spawnThreshold;
    bool hasSpawnEnemies = false;
    CampManager CampManager;
    void Start()
    {
        CampManager = GameObject.Find("GameManager").GetComponent<CampManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if((transform.position - target.position).magnitude < spawnThreshold)
        {
            if(!hasSpawnEnemies)
                SpawnEnemies();
        }
    }

    void SpawnEnemies()
    {
        hasSpawnEnemies = true;
        CampModel campModel = GetComponent<CampModel>();

        if (campModel.Enemies.Count <=   0)
        {
            for (int i = 0; i < campModel.numTotalEnemies; i++)
            {
                campModel.Enemies.Add(CampManager.enemiesPossible[Random.Range(0, CampManager.enemiesPossible.Count)]);
            }
        }
        List<GameObject> enemies = campModel.Enemies;
        List<Vector3> positions = campModel.EnemiesSpawnPositions;
        for (int i = 0; i < enemies.Count; i++)
        {
            if (enemies[i] != null && positions[i]!=null)
            {
                Instantiate(enemies[i], positions[i], Quaternion.identity, transform);
            }
        }
    }
}
