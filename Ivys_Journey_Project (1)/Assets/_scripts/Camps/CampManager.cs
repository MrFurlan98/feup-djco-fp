using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampManager : MonoBehaviour
{
    public List<GameObject> allCamps;
    public List<GameObject> enemiesPossible;
    public LayerMask terrainMask;

    private void Start()
    {
        foreach(GameObject camp in allCamps)
        {
            CampModel campModel = camp.GetComponent<CampModel>();
            Vector3 pos;

            for (int i = 0; i < campModel.numTotalEnemies; i++)
            {
                float radius = Random.Range(0, 50f);
                float angle = Random.Range(0, 360f);
                pos = new Vector3(camp.transform.position.x + radius * Mathf.Cos(angle), camp.transform.localPosition.y+2, camp.transform.position.z + radius * Mathf.Sin(angle));
                campModel.EnemiesSpawnPositions.Add(pos);
            }
        }
    }
}
