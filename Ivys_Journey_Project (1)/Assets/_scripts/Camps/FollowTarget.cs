using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{
    [SerializeField]
    public Transform target;
    public float angleXOffset;
    public float angleYOffset;
    public float angleZOffset;
    private void Start()
    {
        target = GameObject.Find("Player").transform;
    }
    public enum RotationAxis
    {
        xAxis,
        YAxis,
        ZAxis
    }

    public RotationAxis rotationAxis;
    private void Update()
    {
        Vector3 dir = Vector3.one;
        switch (rotationAxis)
        {
            case RotationAxis.xAxis:
                float angleX = Vector3.SignedAngle(new Vector3(target.position.x, transform.position.y, target.position.z) - transform.position, Vector3.forward, Vector3.up);
                dir = new Vector3(angleXOffset + angleX, angleYOffset, angleZOffset);
                transform.eulerAngles = dir;
                break;
            case RotationAxis.YAxis:
                float angleY = Vector3.SignedAngle(new Vector3(target.position.x, transform.position.y, target.position.z) - transform.position, Vector3.forward, Vector3.up);
                dir = new Vector3(angleXOffset, -(angleYOffset + angleY), angleZOffset);
                transform.eulerAngles = dir;
                break;
            case RotationAxis.ZAxis:
                float angleZ = Vector3.SignedAngle(new Vector3(target.position.x, transform.position.y, target.position.z) - transform.position, Vector3.forward, Vector3.up);
                dir = new Vector3(angleXOffset, angleYOffset, angleZOffset+angleZ);
                transform.eulerAngles = dir;
                break;
        }
    }
}
