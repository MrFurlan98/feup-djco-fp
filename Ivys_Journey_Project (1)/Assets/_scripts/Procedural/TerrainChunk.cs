using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainChunk
{
    public event System.Action<TerrainChunk, bool> onVisibilityChanged;

    public Vector2 coord;

    public List<Vector3> treePositions;
    public List<Vector3> rockPositions;
    public List<Vector3> bushesPositions;
    public List<Vector3> grassPositions;

    const float colliderGenerationDistanceThreshold = 70f;

    GameObject meshObject;
    Vector2 sampleCenter;
    Bounds bounds;

    HeightMap heightMap;
    bool heightMapReceived;
    int previousLODIndex = -1;
    int seed;
    bool hasSetCollider = false;
    bool hasSpawnTrees = false;
    bool hasSpawnRocks = false;
    bool hasSpawnBushes = false;
    bool hasSpawnGrass = false;
    float maxViewDistance;

    MeshRenderer meshRenderer;
    MeshFilter meshFilter;
    MeshCollider meshCollider;

    LODInfo[] detailLevels;
    LODMesh[] lodMeshes;
    int colliderLODIndex;

    HeightMapSettings heightMapSettings;
    MeshSettings meshSettings;
    EnvironmentSettings environmentSettings;

    Transform viewer;

    public TerrainChunk(Vector2 coord, HeightMapSettings heightMapSettings, MeshSettings meshSettings, EnvironmentSettings environmentSettings, LODInfo[] detailLevels, int colliderLODIndex, Transform parent,Transform viewer, Material mat)
    {
        this.coord = coord;
        this.detailLevels = detailLevels;
        this.colliderLODIndex = colliderLODIndex;
        this.heightMapSettings = heightMapSettings;
        this.meshSettings = meshSettings;
        this.environmentSettings = environmentSettings;
        this.viewer = viewer;
        this.seed = environmentSettings.seed;

        sampleCenter = coord * meshSettings.meshWorldSize / meshSettings.meshScale;
        Vector2 position = coord * meshSettings.meshWorldSize;


        bounds = new Bounds(position, Vector2.one * meshSettings.meshWorldSize);

        meshObject = new GameObject("Terrain Chunk");
        meshRenderer = meshObject.AddComponent<MeshRenderer>();
        meshFilter = meshObject.AddComponent<MeshFilter>();
        meshCollider = meshObject.AddComponent<MeshCollider>();
        meshRenderer.material = mat;
        meshObject.transform.position = new Vector3(position.x, 0, position.y);
        meshObject.transform.parent = parent;
        meshObject.layer = 7;

        GrabCamps();

        SetVisible(false);

        lodMeshes = new LODMesh[detailLevels.Length];

        for (int i = 0; i < detailLevels.Length; i++)
        {
            lodMeshes[i] = new LODMesh(detailLevels[i].lod);
            lodMeshes[i].updateCallback += UpdateTerrain;
            if (i == colliderLODIndex)
            {
                lodMeshes[i].updateCallback += UpdateCollisionMesh;
                lodMeshes[i].updateCallback += SpawnTree;
                lodMeshes[i].updateCallback += SpawnRock;
                lodMeshes[i].updateCallback += SpawnBush;
                //lodMeshes[i].updateCallback += SpawnGrass;
            }
        }
        maxViewDistance = detailLevels[detailLevels.Length - 1].visibleDstThreshold;

        
    }

    public void Load()
    {
        ThreadedDataRequester.RequestData(() => HeightMapGenerator.GenerateHeightMap(meshSettings.numVerticesPerLine, meshSettings.numVerticesPerLine, heightMapSettings, sampleCenter), OnHeightMapReceived);
    }

    void OnHeightMapReceived(object heightMapObject)
    {
        this.heightMap = (HeightMap)heightMapObject;
        heightMapReceived = true;
        UpdateTerrain();
    }

    Vector2 viewerPos
    {
        get
        {
            return new Vector2(viewer.position.x, viewer.position.z);
        }
    }

    public void UpdateTerrain()
    {
        if (heightMapReceived)
        {
            float viewerDstFromNearestEdge = Mathf.Sqrt(bounds.SqrDistance(viewerPos));
            bool wasVisible = IsVisible();
            bool isVisible = viewerDstFromNearestEdge <= maxViewDistance;

            if (isVisible)
            {
                int lodIndex = 0;
                for (int i = 0; i < detailLevels.Length - 1; i++)
                {
                    if (viewerDstFromNearestEdge > detailLevels[i].visibleDstThreshold)
                        lodIndex = i + 1;
                    else
                        break;
                }
                if (lodIndex != previousLODIndex)
                {
                    LODMesh lodMesh = lodMeshes[lodIndex];
                    if (lodMesh.hasMesh)
                    {
                        previousLODIndex = lodIndex;
                        meshFilter.mesh = lodMesh.mesh;
                    }
                    else if (!lodMesh.hasRequestedMesh)
                    {
                        lodMesh.RequestMesh(heightMap,meshSettings);
                    }
                }
            }
            if (wasVisible != isVisible)
            {
                SetVisible(isVisible);
                if (onVisibilityChanged != null)
                {
                    onVisibilityChanged(this, isVisible);    
                }                    
            }
        }
    }

    public void UpdateCollisionMesh()
    {
        if (!hasSetCollider)
        {
            float sqrDstFromViewerToEdge = bounds.SqrDistance(viewerPos);

            if (sqrDstFromViewerToEdge < detailLevels[colliderLODIndex].sqrVisibleDstThreshold)
            {
                if (!lodMeshes[colliderLODIndex].hasRequestedMesh)
                {
                    lodMeshes[colliderLODIndex].RequestMesh(heightMap,meshSettings);
                }
            }
            if (sqrDstFromViewerToEdge < colliderGenerationDistanceThreshold * colliderGenerationDistanceThreshold)
            {
                if (lodMeshes[colliderLODIndex].hasMesh)
                {
                    meshCollider.sharedMesh = lodMeshes[colliderLODIndex].mesh;
                    hasSetCollider = true;
                }
            }
            SpawnTree();
            SpawnRock();
            SpawnBush();
            //SpawnGrass();
        }
    }

    void GrabCamps()
    {
        GameObject gameManager = GameObject.Find("GameManager");
        foreach (GameObject camp in gameManager.GetComponent<CampManager>().allCamps)
        {
            if(!(camp.transform.position.x < meshObject.transform.position.x - meshSettings.meshWorldSize / 2 || 
                camp.transform.position.x > meshObject.transform.position.x + meshSettings.meshWorldSize / 2 ||
                camp.transform.position.z < meshObject.transform.position.z - meshSettings.meshWorldSize / 2 ||
                camp.transform.position.z > meshObject.transform.position.z + meshSettings.meshWorldSize / 2))
            {
                camp.transform.parent = meshObject.transform;
            }
        }
    }
    private void SpawnTree()
    {
        if (!hasSpawnTrees && hasSetCollider)
        {
            Vector2 position = coord * meshSettings.meshWorldSize;
            treePositions = PoissonDiscSampler.GeneratePoints(environmentSettings, heightMapSettings.maxHeight, (int)meshSettings.meshWorldSize, new Vector3(position.x, 0, position.y),0);
            if (treePositions.Count == 0)
                return;
            Random.InitState(seed);
            foreach (Vector3 treePos in treePositions)
            {
                GameObject tree = GameObject.Instantiate(environmentSettings.trees[Random.Range(0,environmentSettings.trees.Count)], treePos, Quaternion.identity, meshObject.transform);
                tree.transform.eulerAngles = new Vector3(0, Random.Range(0, 360), 0);
                tree.transform.localScale *= Random.Range(1.5f, 2f);
            }
            hasSpawnTrees = true;
        }
    }
    private void SpawnRock()
    {
        if (!hasSpawnRocks && hasSetCollider)
        {
            Vector2 position = coord * meshSettings.meshWorldSize;
            rockPositions = PoissonDiscSampler.GeneratePoints(environmentSettings, heightMapSettings.maxHeight, (int)meshSettings.meshWorldSize, new Vector3(position.x, 0, position.y),1);
            if (rockPositions.Count == 0)
                return;
            Random.InitState(seed);
            foreach (Vector3 rockPos in rockPositions)
            {
                GameObject rock = GameObject.Instantiate(environmentSettings.rocks[Random.Range(0,environmentSettings.rocks.Count)], rockPos, Quaternion.identity, meshObject.transform);
                rock.transform.eulerAngles = new Vector3(0, Random.Range(0, 360), 0);
                rock.transform.localScale *= Random.Range(0.3f, 0.7f);
            }
            hasSpawnRocks = true;
        }
    }

    private void SpawnBush()
    {
        if (!hasSpawnBushes && hasSetCollider)
        {
            Vector2 position = coord * meshSettings.meshWorldSize;
            bushesPositions = PoissonDiscSampler.GeneratePoints(environmentSettings, heightMapSettings.maxHeight, (int)meshSettings.meshWorldSize, new Vector3(position.x, 0, position.y), 2);
            if (bushesPositions.Count == 0)
                return;
            Random.InitState(seed);
            foreach (Vector3 bushPos in bushesPositions)
            {
                GameObject bush = GameObject.Instantiate(environmentSettings.bushes[Random.Range(0, environmentSettings.bushes.Count)], bushPos, Quaternion.identity, meshObject.transform);
                bush.transform.eulerAngles = new Vector3(0, Random.Range(0, 360), 0);
                bush.transform.localScale *= Random.Range(0.3f, 0.7f);
            }
            hasSpawnBushes = true;
        }
    }
    private void SpawnGrass()
    {
        if (!hasSpawnGrass && hasSetCollider)
        {
            Vector2 position = coord * meshSettings.meshWorldSize;
            grassPositions = PoissonDiscSampler.GeneratePoints(environmentSettings, heightMapSettings.maxHeight, (int)meshSettings.meshWorldSize, new Vector3(position.x, 0, position.y), 3);
            if (grassPositions.Count == 0)
                return;
            Random.InitState(seed);
            foreach (Vector3 grassPos in grassPositions)
            {
                GameObject grass = GameObject.Instantiate(environmentSettings.grass[Random.Range(0, environmentSettings.grass.Count)], grassPos, Quaternion.identity, meshObject.transform);
                grass.transform.eulerAngles = new Vector3(0, Random.Range(0, 360), 0);
                grass.transform.localScale *= Random.Range(0.3f, 0.7f);
            }
            hasSpawnGrass = true;
        }
    }
    public void SetVisible(bool isVisible)
    {
        meshObject.SetActive(isVisible);
    }

    public bool IsVisible()
    {
        return meshObject.activeSelf;
    }

}
class LODMesh
{
    public Mesh mesh;
    public bool hasRequestedMesh;
    public bool hasMesh;
    int lod;
    public event System.Action updateCallback;

    public LODMesh(int lod)
    {
        this.lod = lod;
    }
    void OnMeshDataReceived(object meshDataObject)
    {
        mesh = ((MeshData)meshDataObject).CreateMesh();
        hasMesh = true;
        updateCallback();
    }
    public void RequestMesh(HeightMap heightMap, MeshSettings meshSettings)
    {
        hasRequestedMesh = true;
        ThreadedDataRequester.RequestData(() => MeshGenerator.GenerateMesh(heightMap.values, meshSettings, lod), OnMeshDataReceived);
    }
}
