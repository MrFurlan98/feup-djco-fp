using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Noise
{
    public enum NormalizeMode { Local, Global};
    public static float[,] GenerateNoiseMap(int width, int height, NoiseSettings settings, Vector2 sampleCenter)
    {
        float[,] noiseMap = new float[width, height];

        System.Random rng = new System.Random(settings.seed);
        Vector2[] octaveOffsets = new Vector2[settings.octaves];

        float maxPossibleHeight = 0;
        float amp = 1f;
        float freq = 1f;

        for (int i = 0; i < settings.octaves; i++)
        {
            float offsetX = rng.Next(-100000, 100000) + settings.offset.x + sampleCenter.x;
            float offsetY = rng.Next(-100000, 100000) - settings.offset.y - sampleCenter.y;
            octaveOffsets[i] = new Vector2(offsetX, offsetY);

            maxPossibleHeight += amp;
            amp *= settings.persistance;
        }

        float maxLocalHeight = float.MinValue;
        float minLocalHeight = float.MaxValue;

        float halfWidth = width / 2f;
        float halfHeight = height / 2f;

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                amp = 1f;
                freq = 1f;
                float noiseHeight = 0;

                for (int i = 0; i < settings.octaves; i++)
                {
                    float sampleX = (x - halfWidth + octaveOffsets[i].x) / settings.scale * freq;
                    float sampleY = (y - halfHeight + octaveOffsets[i].y) / settings.scale * freq;

                    float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
                    noiseHeight += perlinValue * amp;

                    amp *= settings.persistance;
                    freq *= settings.lacunarity;
                }

                if (noiseHeight > maxLocalHeight)
                    maxLocalHeight = noiseHeight;
                if (noiseHeight < minLocalHeight)
                    minLocalHeight = noiseHeight;

                noiseMap[x, y] = noiseHeight;
                if(settings.normalizeMode == NormalizeMode.Global)
                {
                    float normalizedHeight = (noiseMap[x, y] + 1) / (maxPossibleHeight / .9f);
                    noiseMap[x, y] = Mathf.Clamp(normalizedHeight, 0, int.MaxValue);
                }
            }
        }

        if (settings.normalizeMode == NormalizeMode.Local)
        {
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    noiseMap[x, y] = Mathf.InverseLerp(minLocalHeight, maxLocalHeight, noiseMap[x, y]);
                }
            }
        }    
        return noiseMap;
    }
}

[System.Serializable]
public class NoiseSettings{
    public Noise.NormalizeMode normalizeMode;

    public float scale = 50;

    public int octaves = 6;
    [Range(0, 1)]
    public float persistance = 0.471f;
    public float lacunarity=2;

    public int seed=0;
    public Vector2 offset;

    public void ValidateValues()
    {
        scale = Mathf.Max(scale, 0.01f);
        octaves = Mathf.Max(octaves, 1);
        lacunarity = Mathf.Max(lacunarity, 1);
        persistance = Mathf.Clamp01(persistance);
    }
}