using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class TerrainGenerator : MonoBehaviour
{
    const float viewerMoveThresholdForChunkUpdate = 25f;
    const float sqrtViewerMoveThresholdForChunkUpdate = viewerMoveThresholdForChunkUpdate * viewerMoveThresholdForChunkUpdate;

    public int colliderLODIndex;
    public LODInfo[] detailLevels;

    public MeshSettings meshSettings;
    public HeightMapSettings heightMapSettings;
    public EnvironmentSettings environmentSettings;
    public TextureData textureSettings;

    public Transform viewer;
    public Material mapMaterial;

    public Vector2 viewerPos;
    Vector2 viewerPosOld;
    float meshWorldSize;
    int chunckVisible;
    [SerializeField]
    Dictionary<Vector2, TerrainChunk> terrainChunckDictionary = new Dictionary<Vector2, TerrainChunk>();
    [SerializeField]
    List<TerrainChunk> terrainChunksVisibleLastUpdate = new List<TerrainChunk>();
    
    private void Start()
    {
        textureSettings.ApplyToMaterial(mapMaterial);
        textureSettings.UpdateMeshHeights(mapMaterial, heightMapSettings.minHeight, heightMapSettings.maxHeight);
        float maxViewDistance = detailLevels[detailLevels.Length - 1].visibleDstThreshold;
        meshWorldSize = meshSettings.meshWorldSize;
        chunckVisible = Mathf.RoundToInt(maxViewDistance / meshWorldSize);
        UpdateVisibleChuncks();
    }
    private void Update()
    {
        viewerPos = new Vector2(viewer.position.x, viewer.position.z);

        if (viewerPos != viewerPosOld)
        {
            foreach (TerrainChunk chunk in terrainChunksVisibleLastUpdate)
            {
                chunk.UpdateCollisionMesh();
            }
        }
        if ((viewerPosOld-viewerPos).sqrMagnitude > sqrtViewerMoveThresholdForChunkUpdate)
        {
            viewerPosOld = viewerPos;
            UpdateVisibleChuncks();
        }
    }

    

    void UpdateVisibleChuncks()
    {
        HashSet<Vector2> alreadyUpdatedCoords = new HashSet<Vector2>();
        for (int i = terrainChunksVisibleLastUpdate.Count -1; i >= 0; i--)
        {
            alreadyUpdatedCoords.Add(terrainChunksVisibleLastUpdate[i].coord);
            terrainChunksVisibleLastUpdate[i].UpdateTerrain();
        }
        int currentChunckCoordX = Mathf.RoundToInt(viewerPos.x / meshWorldSize);
        int currentChunckCoordY = Mathf.RoundToInt(viewerPos.y / meshWorldSize);

        for (int yOffset = -chunckVisible; yOffset <= chunckVisible; yOffset++)
        {
            for (int xOffset = -chunckVisible; xOffset <= chunckVisible; xOffset++)
            {
                Vector2 viewedChunckCoord = new Vector2(currentChunckCoordX + xOffset, currentChunckCoordY + yOffset);

                if (!alreadyUpdatedCoords.Contains(viewedChunckCoord))
                {
                    if (terrainChunckDictionary.ContainsKey(viewedChunckCoord))
                    {
                        terrainChunckDictionary[viewedChunckCoord].UpdateTerrain();
                    }
                    else
                    {
                        TerrainChunk newChunk = new TerrainChunk(viewedChunckCoord, heightMapSettings, meshSettings, environmentSettings, detailLevels, colliderLODIndex, transform, viewer, mapMaterial);
                        terrainChunckDictionary.Add(viewedChunckCoord, newChunk);
                        newChunk.onVisibilityChanged += OnTerrainChunkVisibilityChanged;
                        newChunk.Load();
                    }
                }  
            }
        }
    }

    void OnTerrainChunkVisibilityChanged(TerrainChunk chunk, bool isVisible)
    {
        if (isVisible)
        {
            terrainChunksVisibleLastUpdate.Add(chunk);
        }
        else
        {
            terrainChunksVisibleLastUpdate.Remove(chunk);
        }
    }
}

[System.Serializable]
public struct LODInfo
{
    [Range(0, MeshSettings.numSupportedLODs - 1)]
    public int lod;
    public float visibleDstThreshold;

    public float sqrVisibleDstThreshold
    {
        get
        {
            return visibleDstThreshold * visibleDstThreshold;
        }
    }
}
