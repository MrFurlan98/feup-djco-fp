using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class EnvironmentSettings : UpdatableData
{
    public int seed = 0;
    [Header("Trees Settings")]
    #region trees
    public float treeRadius = 5;
    public int treeRejectionSamples = 30;
    public Vector2 treeHeightRange;
    public List<GameObject> trees;
    #endregion

    [Header("Rocks Settings")]
    #region Rocks
    public float rockRadius = 5;
    public int rockRejectionSamples = 30;
    public Vector2 rockHeightRange;
    public List<GameObject> rocks;
    #endregion

    [Header("Bushes Settings")]
    #region Bush
    public float bushRadius = 5;
    public int bushRejectionSamples = 30;
    public Vector2 bushHeightRange;
    public List<GameObject> bushes;
    #endregion
    
    [Header("Grass Settings")]
    #region Grass
    public float grassRadius = 5;
    public int grassRejectionSamples = 30;
    public Vector2 grassHeightRange;
    public List<GameObject> grass;
    #endregion
}

