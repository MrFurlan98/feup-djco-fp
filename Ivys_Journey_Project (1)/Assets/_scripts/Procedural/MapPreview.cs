using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MapPreview : MonoBehaviour
{
    public enum DrawMode { FallOff, NoiseMap, Mesh };
    public DrawMode drawMode;

    public MeshSettings meshSettings;
    public HeightMapSettings heightMapSettings;
    public EnvironmentSettings environmentSettings;
    public TextureData textureData;

    public Material terrainMaterial;

    [Range(0, MeshSettings.numSupportedLODs - 1)]
    public int editorLOD;

    public bool autoUpdate;

    public Renderer rend;
    public MeshFilter meshFilter;
    public MeshRenderer meshRenderer;

    public Vector3 regionSize;
    public float displayRadius = 1;
    public Vector3 center;

    List<Vector3> treePoints;
    List<Vector3> rocksPoints;
    List<Vector3> bushesPoints;
    List<Vector3> grassPoints;
    public void Display(Texture2D texture)
    {
        rend.sharedMaterial.mainTexture = texture;
        rend.transform.localScale = new Vector3(texture.width, 1, texture.height) / 10f;

        rend.gameObject.SetActive(true);
        meshRenderer.gameObject.SetActive(false);
    }

    public void DrawMesh(MeshData meshData)
    {
        meshFilter.sharedMesh = meshData.CreateMesh();
        rend.gameObject.SetActive(false);
        meshRenderer.gameObject.SetActive(true);
    }

    void OnValuesUpdated()
    {
        if (!Application.isPlaying && !EditorApplication.isCompiling)
            DrawMapInEditor();
    }

    void OnTextureValuesUpdated()
    {
        textureData.ApplyToMaterial(terrainMaterial);
    }

    public void DrawMapInEditor()
    {
        textureData.ApplyToMaterial(terrainMaterial);
        textureData.UpdateMeshHeights(terrainMaterial, heightMapSettings.minHeight, heightMapSettings.maxHeight);
        HeightMap heightMap = HeightMapGenerator.GenerateHeightMap(meshSettings.numVerticesPerLine, meshSettings.numVerticesPerLine, heightMapSettings, Vector2.zero);
        if (drawMode == DrawMode.NoiseMap)
            Display(TextureGenerator.TextureFromNoiseMap(heightMap));
        else if (drawMode == DrawMode.Mesh)
            DrawMesh(MeshGenerator.GenerateMesh(heightMap.values, meshSettings, editorLOD));
        else if (drawMode == DrawMode.FallOff)
            Display(TextureGenerator.TextureFromNoiseMap(new HeightMap(FallOffGenerator.GenerateFallOffMap(meshSettings.numVerticesPerLine),0,1)));
        treePoints = PoissonDiscSampler.GeneratePoints(environmentSettings, heightMapSettings.maxHeight, (int) meshSettings.meshWorldSize, center,0);
        rocksPoints = PoissonDiscSampler.GeneratePoints(environmentSettings, heightMapSettings.maxHeight, (int) meshSettings.meshWorldSize, center,1);
        bushesPoints = PoissonDiscSampler.GeneratePoints(environmentSettings, heightMapSettings.maxHeight, (int) meshSettings.meshWorldSize, center,2);
        //grassPoints = PoissonDiscSampler.GeneratePoints(environmentSettings, heightMapSettings.maxHeight, (int) meshSettings.meshWorldSize, center,3);
    }

    private void OnValidate()
    {
        if (meshSettings != null)
        {
            meshSettings.OnValuesUpdated -= OnValuesUpdated;
            meshSettings.OnValuesUpdated += OnValuesUpdated;
        }
        if (heightMapSettings != null)
        {
            heightMapSettings.OnValuesUpdated -= OnValuesUpdated;
            heightMapSettings.OnValuesUpdated += OnValuesUpdated;
        }
        if (textureData != null)
        {
            textureData.OnValuesUpdated -= OnValuesUpdated;
            textureData.OnValuesUpdated += OnValuesUpdated;
        }
        if (environmentSettings != null)
        {
            environmentSettings.OnValuesUpdated -= OnValuesUpdated;
            environmentSettings.OnValuesUpdated += OnValuesUpdated;
        }

    }

    void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(Vector3.zero, regionSize);
        if (treePoints != null)
        {
            Gizmos.color = Color.red;
            foreach (Vector3 point in treePoints)
            {
                Gizmos.DrawSphere(point, displayRadius);
            }
        }
        if (rocksPoints != null)
        {
            Gizmos.color = Color.blue;
            foreach (Vector3 point in rocksPoints)
            {
                Gizmos.DrawSphere(point, displayRadius);
            }
        }
        if (bushesPoints != null)
        {
            Gizmos.color = Color.yellow;
            foreach (Vector3 point in bushesPoints)
            {
                Gizmos.DrawSphere(point, displayRadius);
            }
        }
        /*if (grassPoints != null)
        {
            Gizmos.color = Color.magenta;
            foreach (Vector3 point in grassPoints)
            {
                Gizmos.DrawSphere(point, displayRadius);
            }
        }*/
    }
}
