using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading;

//[ExecuteInEditMode]
public class ThreadedDataRequester : MonoBehaviour
{
    static ThreadedDataRequester instance;

    private void Awake()
    {
        instance = FindObjectOfType<ThreadedDataRequester>();
    }

    Queue<ThreadInfo> dataQueue = new Queue<ThreadInfo>();
    public static void RequestData(Func<object> generateData, Action<object> callback)
    {
        ThreadStart threadStart = delegate
        {
            instance.DataThread(generateData, callback);
        };

        new Thread(threadStart).Start();
    }

    void DataThread(Func<object> generateData, Action<object> callback)
    {
        object data = generateData();
        lock (dataQueue)
        {
            dataQueue.Enqueue(new ThreadInfo(callback, data));
        }
    }

    private void Update()
    {
        if (dataQueue.Count > 0)
        {
            for (int i = 0; i < dataQueue.Count; i++)
            {
                ThreadInfo threadInfo = dataQueue.Dequeue();
                threadInfo.callback(threadInfo.parameter);
            }
        }
    }
    struct ThreadInfo
    {
        public readonly Action<object> callback;
        public readonly object parameter;

        public ThreadInfo(Action<object> callback, object parameter) : this()
        {
            this.callback = callback;
            this.parameter = parameter;
        }
    }
}
