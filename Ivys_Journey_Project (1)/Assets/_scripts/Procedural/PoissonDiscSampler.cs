using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PoissonDiscSampler
{
    private static float radius = 5;
    private static int rejectionSamples = 30;
    private static Vector2 heightRange;
    public static List<Vector3> GeneratePoints(EnvironmentSettings environmentSettings ,float maxHeight, int sampleRegionSize, Vector3 center, int index)
    {
        SetValeus(environmentSettings, index);
        float cellSize = radius / Mathf.Sqrt(2);
        int[,] grid = new int[Mathf.CeilToInt((sampleRegionSize) / cellSize), Mathf.CeilToInt((sampleRegionSize) / cellSize)];
        List<Vector3> points = new List<Vector3>();
        List<Vector3> spawnPoints = new List<Vector3>();

        Random.InitState(environmentSettings.seed);

        spawnPoints.Add(center);
        int count = 0;
        while (spawnPoints.Count > 0 && count < 10000)
        {
            count++;
            int spawnIndex = Random.Range(0, spawnPoints.Count);
            Vector3 spawnCenter = spawnPoints[spawnIndex];
            bool candidateAccepted = false;
            for (int i = 0; i < rejectionSamples; i++)
            {
                float angle = Random.value * Mathf.PI * 2;
                Vector3 dir = new Vector3(Mathf.Sin(angle),0, Mathf.Cos(angle));
                Vector3 candidate = spawnCenter + dir * Random.Range(radius, 2 * radius);
                if (isValid(candidate,center, sampleRegionSize, cellSize, radius, points, grid))
                {
                    points.Add(candidate);
                    spawnPoints.Add(candidate);
                    grid[(int)((candidate.x - center.x + sampleRegionSize/2) / cellSize), (int)((candidate.z - center.z + sampleRegionSize/2) / cellSize)] = points.Count;
                    candidateAccepted = true;
                    break;
                }
            }
            if (!candidateAccepted)
            {
                spawnPoints.RemoveAt(spawnIndex);
            }
        }
        return SetHeights(points,maxHeight,heightRange);
    }

    static bool isValid(Vector3 candidate,Vector3 center, int sampleRegionSize, float cellSize, float radius, List<Vector3> points, int[,] grid)
    {
        if(candidate.x>=(-sampleRegionSize)/2+center.x && candidate.x < (sampleRegionSize / 2) + center.x && candidate.z >= (-sampleRegionSize / 2) + center.z && candidate.z < (sampleRegionSize / 2) + center.z)
        {
            int cellX = (int)((candidate.x + sampleRegionSize/2 - center.x) /cellSize);
            int cellY = (int)((candidate.z + sampleRegionSize/2 - center.z) /cellSize);
            int searchStartX = Mathf.Max(0, cellX - 2);
            int searchEndX = Mathf.Min(cellX + 2, grid.GetLength(0) - 1);
            int searchStartY = Mathf.Max(0, cellY - 2);
            int searchEndY = Mathf.Min(cellY + 2, grid.GetLength(1) - 1);

            for (int x = searchStartX; x <= searchEndX; x++)
            {
                for (int y = searchStartY; y <= searchEndY; y++)
                {
                    int pointIndex = grid[x, y] - 1;
                    if (pointIndex != -1)
                    {
                        float sqrDst = (candidate - points[pointIndex]).sqrMagnitude;
                        if (sqrDst < radius * radius)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

    static void SetValeus(EnvironmentSettings environmentSettings, int index)
    {
        switch (index)
        {
            case 0:
                radius = environmentSettings.treeRadius;
                rejectionSamples = environmentSettings.treeRejectionSamples;
                heightRange = environmentSettings.treeHeightRange;
                break;
            case 1:
                radius = environmentSettings.rockRadius;
                rejectionSamples = environmentSettings.rockRejectionSamples;
                heightRange = environmentSettings.rockHeightRange;
                break;
            case 2:
                radius = environmentSettings.bushRadius;
                rejectionSamples = environmentSettings.bushRejectionSamples;
                heightRange = environmentSettings.bushHeightRange;
                break;
            case 3:
                radius = environmentSettings.grassRadius;
                rejectionSamples = environmentSettings.grassRejectionSamples;
                heightRange = environmentSettings.grassHeightRange;
                break;
        }
    }

    static List<Vector3> SetHeights(List<Vector3> points, float maxHeight, Vector2 range)
    {
        List<Vector3> newPoints = new List<Vector3>();
        foreach  (Vector3 point in points)
        {
            Vector3 newPoint = new Vector3(point.x, maxHeight, point.z);
            RaycastHit hit;
            //Debug.DrawRay(point, Vector3.up * maxHeight, Color.yellow,100);
            if (Physics.Raycast(newPoint, Vector3.down, out hit, maxHeight))
            {
                if (hit.point.y > range.x * maxHeight && hit.point.y <= range.y * maxHeight)
                {
                    newPoints.Add(hit.point);
                }
               
            }

        }
        return newPoints;
    }
}
