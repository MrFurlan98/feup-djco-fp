using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonInfo : MonoBehaviour
{
    public int ItemID;
    public Text Price_txt;
    public Text Quantity_txt;
    public GameObject ShopManager;

    void Update()
    {
        Price_txt.text = "Price: " + ShopManager.GetComponent<ShopManager>().shopItems[2, ItemID].ToString() + "�";
        Quantity_txt.text = ShopManager.GetComponent<ShopManager>().shopItems[3, ItemID].ToString();
    }
}
