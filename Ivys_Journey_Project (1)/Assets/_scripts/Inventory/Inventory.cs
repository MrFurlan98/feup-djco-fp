using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    #region Singleton 
    public static Inventory instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallBack;

    public int space = 10;
    public int maxWeightCapacity = 20;
    public int currentWeightCapacity;
    public List<Item> items = new List<Item>();
    private void Start()
    {
        currentWeightCapacity = 0;
    }
    public bool Add(Item item)
    {
        if (currentWeightCapacity + item.weight <= maxWeightCapacity)
            items.Add(item);
        else
            return false;

        if(onItemChangedCallBack!=null)
            onItemChangedCallBack.Invoke();

        return true;
    }

    public void Remove(Item item)
    {
        items.Remove(item);

        if (onItemChangedCallBack != null)
            onItemChangedCallBack.Invoke();
    }
}
