using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class JellyEffect : MonoBehaviour
{
    public float intensity = 1f;
    public float mass = 1f;
    public float stiffness = 1f;
    [Range(0,1)]
    public float damping = 0.75f;
    private Mesh originalMesh, cloneMesh;
    private MeshRenderer rend;
    private SlimeVertex[] slimeVertex;
    private Vector3[] vertices;
    void Start()
    {
        originalMesh = GetComponent<MeshFilter>().sharedMesh;
        cloneMesh = Instantiate(originalMesh);
        GetComponent<MeshFilter>().sharedMesh = cloneMesh;
        rend = GetComponent<MeshRenderer>();
        slimeVertex = new SlimeVertex[cloneMesh.vertices.Length];
        for (int i = 0; i < cloneMesh.vertices.Length; i++)
        {
            slimeVertex[i] = new SlimeVertex(i, transform.TransformPoint(cloneMesh.vertices[i]));
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        vertices = originalMesh.vertices;
        for (int i = 0; i < slimeVertex.Length; i++)
        {
            Vector3 target = transform.TransformPoint(vertices[slimeVertex[i].id]);
            float inten = (1 - (rend.bounds.max.y - target.y) / rend.bounds.size.y) * intensity;
            slimeVertex[i].Shake(target, mass, stiffness, damping);
            target = transform.InverseTransformPoint(slimeVertex[i].position);
            vertices[slimeVertex[i].id] = Vector3.Lerp(vertices[slimeVertex[i].id], target, inten);
        }
        cloneMesh.vertices = vertices;
    }

    public class SlimeVertex
    {
        public int id;
        public Vector3 position;
        public Vector3 velocity;
        public Vector3 force;
        public SlimeVertex(int _id, Vector3 _position)
        {
            id = _id;
            position = _position;
        }
        public void Shake(Vector3 target, float m, float s, float d)
        {
            force = (target - position) * s;
            velocity = (velocity + force / m) * d;
            position += velocity;
            if ((velocity + force*(1 + 1 / m)).magnitude < 0.001f)
                position = target;
        }
    }
}
