using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FieldOfView : MonoBehaviour
{
    public float viewRadius;
    public float walkRadius;
    public float patrolRadius;
    public float multOfRays;

    public bool isPatrolling = false;
    public bool isChasing = false;
    public bool foundTarget = false;

    [Range(0,360)]
    public float viewAngle;

    public LayerMask targetMask;
    public LayerMask obstacleMask;
    public LayerMask terrainMask;

    private Rigidbody rigid;

    public MeshFilter viewMeshFilter;
    private Mesh viewMesh;

    private void Start()
    {
        viewMesh = new Mesh();
        viewMesh.name = "FOV";
        viewMeshFilter.mesh = viewMesh;
        DOTween.Init(true, true, LogBehaviour.Default);
        rigid = GetComponent<Rigidbody>();
        StartCoroutine(FindTargetRoutine(.5f));
    }
    IEnumerator FindTargetRoutine(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            FindVisibleTarget();
        }
    }

    void FindVisibleTarget()
    {
        Collider[] targetInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, targetMask);
        if (targetInViewRadius.Length > 0)
        {
            Transform target = targetInViewRadius[0].transform;
            Vector3 dirToTarget = (target.position - transform.position).normalized;
            if (foundTarget)
            {
                isChasing = true;
                if (isChasing)
                    StartCoroutine(Chase(dirToTarget));
            }
            else if (Vector3.Angle(transform.forward, dirToTarget) < viewAngle / 2)
            {
                float dstToTarget = Vector3.Distance(transform.position, target.position);
                if (!Physics.Raycast(transform.position + Vector3.up*2, dirToTarget, dstToTarget, obstacleMask))
                {
                    foundTarget = true;
                    isPatrolling = false;
                    if (!isChasing)
                    {
                        isChasing = true;
                        StartCoroutine(Chase(dirToTarget));
                    }
                }
            }
            else
            {
                foundTarget = false;
                isChasing = false;
                if (!isPatrolling)
                {
                    isPatrolling = true;
                    StartCoroutine(Patrol(3f));
                }
            }
        }
        else
        {
            foundTarget = false;
            isChasing = false;
            if (!isPatrolling)
            {
                isPatrolling = true;
                StartCoroutine(Patrol(3f));
            }
        }
    }

    void DrawFOV()
    {
        int stepCount = Mathf.RoundToInt(viewAngle * multOfRays);
        float stepAngleSize = viewAngle / stepCount;
        List<Vector3> viewPoints = new List<Vector3>();
        for (int i = 0; i < stepCount; i++)
        {
            float angle = transform.eulerAngles.y - viewAngle / 2 + stepAngleSize * i;
            ViewCastInfo newViewCast = ViewCast(angle);
            viewPoints.Add(newViewCast.point);
        }

        int vertexCount = viewPoints.Count + 1;
        Vector3[] vertices = new Vector3[vertexCount];
        int[] triangles = new int[(vertexCount - 2) * 3];

        vertices[0] = Vector3.zero;
        for (int i = 0; i < vertexCount-1; i++)
        {
            vertices[i + 1] = transform.InverseTransformPoint(viewPoints[i]);

            if (i < vertexCount - 2)
            {
                triangles[i * 3] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 2;
            }
        }
        viewMesh.Clear();
        viewMesh.vertices = vertices;
        viewMesh.triangles = triangles;
        viewMesh.RecalculateNormals();
    }
    IEnumerator Chase(Vector3 dirToTarget)
    {
        Vector3 pos = transform.position + (dirToTarget*2) + Vector3.up * 100;
        RaycastHit hit;
        Vector3 truePos = pos;
        if (Physics.Raycast(pos, Vector3.down, out hit, 1000, terrainMask))
        {
            truePos.y = hit.point.y + transform.localScale.y / 2;
            transform.DOLookAt(truePos, .2f);
            rigid.DOJump(truePos, 2, 1, .5f);
        }
        yield return new WaitForSeconds(1f);
        isChasing = false;
    }
    IEnumerator Patrol(float delay)
    {
        float angle = Random.Range(0, 360f);
        Vector3 pos = new Vector3(transform.position.x + walkRadius * Mathf.Cos(angle), 100, transform.position.z + walkRadius * Mathf.Sin(angle));
        RaycastHit hit;
        Vector3 truePos = pos;
        if(Physics.Raycast(pos, Vector3.down, out hit, 1000, terrainMask))
        {
            truePos.y = hit.point.y + transform.localScale.y /2;
        }
        float dstFromOrigin = Vector3.Distance(truePos, transform.parent.position);
        if (dstFromOrigin < patrolRadius)
        {
            transform.DOLookAt(truePos, 1);
            rigid.DOJump(truePos, 2, 1, .5f);
        }
        else
        {
            pos = (transform.position + (transform.parent.position - transform.position).normalized) + Vector3.up * 100;
            truePos = pos;
            if (Physics.Raycast(pos, Vector3.down, out hit, 1000, terrainMask))
            {
                truePos.y = hit.point.y + transform.localScale.y / 2;
            }
            transform.DOLookAt(truePos, 1);
            rigid.DOJump(truePos, 2, 1, .4f);
            delay = delay / 2;
        }
        yield return new WaitForSeconds(delay);
        isPatrolling = false;
    }

    ViewCastInfo ViewCast(float globalAngle)
    {
        Vector3 dir = DirFromAngle(globalAngle, true);
        RaycastHit hit;
        if(Physics.Raycast(transform.position,dir,out hit, viewRadius, obstacleMask))
        {
            return new ViewCastInfo(true, hit.point, hit.distance, globalAngle);
        }
        else
        {
            return new ViewCastInfo(false, transform.position + dir * viewRadius, viewRadius, globalAngle);
        }
    }

    public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        if (!angleIsGlobal)
        {
            angleInDegrees += transform.eulerAngles.y;
        }
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }

    public struct ViewCastInfo
    {
        public bool hit;
        public Vector3 point;
        public float dst;
        public float angle;

        public ViewCastInfo(bool hit, Vector3 point, float dst, float angle)
        {
            this.hit = hit;
            this.point = point;
            this.dst = dst;
            this.angle = angle;
        }
    }
}
