using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    InputManager inputManager;
    PlayerManager playerManager;
    PlayerAnimator playerAnimator;

    Rigidbody rigidbody;

    Vector3 moveDirection;
    Transform cameraObject;

    [Header("Falling")]
    public float inAirTimer;
    public float fallingVelocity;
    public float leapingVelocity;
    public float heightOffset = .5f; 
    public LayerMask groundLayer;

    [Header("Movement Flags")]
    public bool isRunning;
    public bool isGrounded;

    [Header("Moviment Speeds")]
    public float walkSpeed = 5;
    public float runSpeed = 8;
    public float rotationSpeed = 15;
    private void Awake()
    {
        inputManager = GetComponent<InputManager>();
        playerManager = GetComponent<PlayerManager>();
        playerAnimator = GetComponentInChildren<PlayerAnimator>();
        rigidbody = GetComponent<Rigidbody>();
        cameraObject = Camera.main.transform;
    }

    public void HandleAllMovement()
    {
        HandleFallAndLand();
        if (playerManager.isInteracting)
            return;
        HandleRotation();
        HandleMovement();
    }

    private void HandleMovement()
    {

        moveDirection = cameraObject.forward * inputManager.verticalInput;
        moveDirection = moveDirection + cameraObject.right * inputManager.horizontalInput;
        moveDirection.Normalize();
        moveDirection.y = 0;

        if(inputManager.moveAmount >= 0.5f && isRunning)
        {
            moveDirection *= runSpeed;
        }
        else
        {
            moveDirection *= walkSpeed;
        }   

        Vector3 movementVelocity = moveDirection;
        rigidbody.velocity = movementVelocity;

    }
    private void HandleRotation()
    {
        Vector3 targetDirection = Vector3.zero;

        targetDirection += cameraObject.forward * inputManager.verticalInput;
        targetDirection += cameraObject.right * inputManager.horizontalInput;
        targetDirection.Normalize();
        targetDirection.y = 0;

        if(targetDirection == Vector3.zero)
        {
            targetDirection = transform.forward;
        }

        Quaternion targetRotation = Quaternion.LookRotation(targetDirection);
        Quaternion playerRotation = Quaternion.Slerp(transform.rotation, targetRotation,rotationSpeed * Time.deltaTime);

        transform.rotation = playerRotation;
    }

    private void HandleFallAndLand()
    {
        RaycastHit hit;

        Vector3 rayCastOrigin = transform.position;
        rayCastOrigin.y += heightOffset;

        if(Physics.SphereCast(rayCastOrigin,0.4f,-Vector3.up,out hit, 10, groundLayer))
        {
            if (!isGrounded && !playerManager.isInteracting)
            {
                playerAnimator.PlayTargetAnimation("Landing", true);
            }

            Debug.Log(hit.transform.gameObject.name);

            inAirTimer = 0;
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }
        if (!isGrounded)
        {
            if (!playerManager.isInteracting)
            {
                playerAnimator.PlayTargetAnimation("Falling", true);
            }
            inAirTimer += Time.deltaTime;
            rigidbody.AddForce(transform.forward * leapingVelocity);
            rigidbody.AddForce(-Vector3.up * fallingVelocity * inAirTimer);
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position + Vector3.up * heightOffset, 0.4f);
    }
}
