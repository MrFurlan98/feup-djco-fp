using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats :CharacterStats
{
    public int maxStamina = 100;
    public int currentStamina { get; private set; }
    private void Awake()
    {
        currentStamina = maxStamina;
    }
    void Start()
    {
        EquipManager.instance.onEquipmentChanged += OnEquipmentChanged;
    }

    void OnEquipmentChanged(Equipment newEquip, Equipment oldEquip)
    {
        if (newEquip != null)
        {
            armor.AddModifier(newEquip.armorModifier);
            damage.AddModifier(newEquip.damageModifier);
        }
        if(oldEquip != null)
        {
            armor.RemoveModifier(oldEquip.armorModifier);
            damage.RemoveModifier(oldEquip.damageModifier);
        }
    }
}
