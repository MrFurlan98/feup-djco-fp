using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipManager : MonoBehaviour
{
    #region Singleton
    public static EquipManager instance;

    private void Awake()
    {
        instance = this;
    }
    #endregion

    Equipment[] currentEquipment;
    Inventory inventory;

    public delegate void OnEquipmentChanged(Equipment newEquip, Equipment oldEquip);
    public OnEquipmentChanged onEquipmentChanged;

    private void Start()
    {
        int slots = System.Enum.GetNames(typeof(EquipSlot)).Length;
        currentEquipment = new Equipment[slots];
        inventory = Inventory.instance;
    }

    public void EquipItem(Equipment newEquip)
    {
        int slotIndex = (int)newEquip.equipSlot;

        Equipment oldItem = null;
        if (currentEquipment[slotIndex] != null)
        {
            oldItem = currentEquipment[slotIndex];
            inventory.Add(oldItem);
        }
        if (onEquipmentChanged != null)
        {
            onEquipmentChanged.Invoke(newEquip,oldItem);
        }
        currentEquipment[slotIndex] = newEquip;
    }
    public void UnequipItem(int slotIndex)
    {
        if (currentEquipment[slotIndex] != null)
        {
            Equipment oldEquip = currentEquipment[slotIndex];
            inventory.Add(oldEquip);

            currentEquipment[slotIndex] = null;
            {
                onEquipmentChanged.Invoke(null, oldEquip);
            }
        }
    }
}
