using UnityEngine;

[CreateAssetMenu(fileName ="New Item",menuName = "Inventory/Item")]
public class Item : ScriptableObject
{
    new public string name = "New Item";
    public Sprite icon = null;
    public int weight = 0;
    public int price = 0;

    public virtual void Use()
    {
        Debug.Log("" + name + " used");
    }

    public void RemoveFromInventory()
    {
        Inventory.instance.Remove(this);
    }
}
