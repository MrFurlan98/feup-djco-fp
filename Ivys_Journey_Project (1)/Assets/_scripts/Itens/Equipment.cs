using UnityEngine;
[CreateAssetMenu(fileName ="New Equipment",menuName = "Inventory/Equipment")]
public class Equipment : Item
{
    public EquipSlot equipSlot;
    public int armorModifier;
    public int damageModifier;

    public override void Use()
    {
        base.Use();
        EquipManager.instance.EquipItem(this);
        RemoveFromInventory();
    }
}

public enum EquipSlot
{
    Head,
    Chest,
    Legs,
    Feet,
    Weapon
}