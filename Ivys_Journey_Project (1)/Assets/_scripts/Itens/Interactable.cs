using UnityEngine;

public class Interactable : MonoBehaviour
{
    public float radius = 3f;
    public LayerMask targetMask;
    public GameObject popUp;

    private void Start()
    {
        popUp = Instantiate(popUp, transform.position + Vector3.up * transform.localScale.y, Quaternion.identity, transform);
        popUp.SetActive(false);
    }
    private void Update()
    {
        if (Physics.CheckSphere(transform.position, radius, targetMask))
        {
            popUp.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E))
            {
                DoInteraction();
            }
        }
        else
        {
            popUp.SetActive(false);
        }
    }
    public virtual void DoInteraction()
    {
        Debug.Log("making interaction");
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
