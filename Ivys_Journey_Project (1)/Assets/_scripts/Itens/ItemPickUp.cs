using UnityEngine;
public class ItemPickUp : Interactable
{
    public Item item;
    public override void DoInteraction()
    {
        base.DoInteraction();

        PickUp();
    }

    private void PickUp()
    {
        Debug.Log("picking item");
        if(Inventory.instance.Add(item))
            Destroy(gameObject);
    }
}
