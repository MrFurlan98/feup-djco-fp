using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Consumable", menuName = "Inventory/Consumable")]
public class Consumable : Item
{
    public float time;
    public int armorModifier;
    public int damageModifier;
    public int healthModifier;
    public int staminaModifier;

    public override void Use()
    {
        base.Use();
    }
}
